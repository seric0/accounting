import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../views/HomeView.vue')
  },
  {
    path: '/purchase',
    name: 'purchase',    
    component: () => import('../views/PurchaseView.vue')
  },  
  {
    path: '/finance',
    name: 'finance',    
    component: () => import('../views/FinanceView.vue')
  },
  {
    path: '/statistic',
    name: 'statistic',    
    component: () => import('../views/StatisticView.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
